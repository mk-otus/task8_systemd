# Otus_task8
Task 1
Пишем сервис, который будет раз в 30 секунд мониторить лог на предмет наличия ключевого слова
Сделал файлы как в инструкции все работает
```
/etc/sysconfig/watchlog
/var/log/watchlog.log
/etc/systemd/system/watchlog.service
/etc/systemd/system/watchlog.timer
```
В логах вижу работу таймера и сервиса
 ```
[root@localhost vagrant]# tail -f /var/log/messages
Sep  7 22:28:10 localhost systemd: Started My watchlog service.
Sep  7 22:28:50 localhost systemd: Starting My watchlog service...
Sep  7 22:28:50 localhost root: Sat Sep  7 22:28:50 UTC 2019: I found word, Master!
Sep  7 22:28:50 localhost systemd: Started My watchlog service.
Sep  7 22:30:10 localhost systemd: Starting My watchlog service...
Sep  7 22:30:10 localhost root: Sat Sep  7 22:30:10 UTC 2019: I found word, Master!
```


Task2
Из epel установить spawn-fcgi и переписать init-скрипт на unit-файл
По инструкции
```
/etc/sysconfig/spawn-fcgi
/etc/systemd/system/spawn-fcgi.service
```
systemctl start/status spawn-fcgi

```
[root@localhost vagrant]# systemctl status spawn-fcgi
● spawn-fcgi.service - Spawn-fcgi startup service by Otus
   Loaded: loaded (/etc/systemd/system/spawn-fcgi.service; disabled; vendor preset: disabled)
   Active: active (running) since Сб 2019-09-07 22:58:25 UTC; 3s ago
 Main PID: 20075 (php-cgi)
   CGroup: /system.slice/spawn-fcgi.service
           ├─20075 /usr/bin/php-cgi
           ├─20076 /usr/bin/php-cgi
           ├─20077 /usr/bin/php-cgi
           ├─20078 /usr/bin/php-cgi
           ├─20079 /usr/bin/php-cgi
           ├─20080 /usr/bin/php-cgi
           ├─20081 /usr/bin/php-cgi
           ├─20082 /usr/bin/php-cgi
```


Task3
Дополним юнит-файл apache httpd возможностью запустить несколько инстансов сервера с разными конфигами

```
переделываем строчку в /usr/lib/systemd/system/httpd.service
EnvironmentFile=/etc/sysconfig/httpd-%I


# /etc/sysconfig/httpd-first
OPTIONS=-f conf/first.conf
# /etc/sysconfig/httpd-second
OPTIONS=-f conf/second.conf


Делаем 2 конфига для httpd - first.conf, seconf.conf (81 port)


mv httpd.service httpd@.service
systemctl start httpd@first
systemctl start httpd@second


[root@localhost system]# ss -tnulp | grep httpd
tcp    LISTEN     0      128      :::80                   :::*                   users:(("httpd",pid=20659,fd=4),("httpd",pid=20658,fd=4),("httpd",pid=20657,fd=4),("httpd",pid=20656,fd=4),("httpd",pid=20655,fd=4),("httpd",pid=20654,fd=4),("httpd",pid=20653,fd=4))
tcp    LISTEN     0      128      :::81                   :::*                   users:(("httpd",pid=20676,fd=4),("httpd",pid=20675,fd=4),("httpd",pid=20674,fd=4),("httpd",pid=20673,fd=4),("httpd",pid=20672,fd=4),("httpd",pid=20671,fd=4),("httpd",pid=20670,fd=4))

```







